package com.aws.appsync;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.amazonaws.amplify.generated.graphql.CreateTodoMutation;
import com.amazonaws.amplify.generated.graphql.ListTodosQuery;
import com.amazonaws.mobile.config.AWSConfiguration;
import com.amazonaws.mobileconnectors.appsync.AWSAppSyncClient;
import com.amazonaws.mobileconnectors.appsync.fetcher.AppSyncResponseFetchers;
import com.apollographql.apollo.GraphQLCall;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;

import javax.annotation.Nonnull;

import type.CreateTodoInput;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();
    private AWSAppSyncClient mAWSAppSyncClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initAWSClient();

        Button btnCreateToDo = findViewById(R.id.btn_create_todo);
        btnCreateToDo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                runMutation();
            }
        });

        Button btnQueryResults = findViewById(R.id.btn_get_results);
        btnQueryResults.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                runQuery();
            }
        });
    }

    private void initAWSClient() {
        if (mAWSAppSyncClient == null) {
            mAWSAppSyncClient = AWSAppSyncClient.builder()
                    .context(getApplicationContext())
                    .awsConfiguration(new AWSConfiguration(getApplicationContext()))
                    .build();
        }
    }

    public void runMutation() {
        CreateTodoInput createTodoInput = CreateTodoInput.builder().
                name("App Sync Mobile Demo 1").
                description("Realtime AppSync").
                build();

        if (mAWSAppSyncClient != null) {
            mAWSAppSyncClient.mutate(CreateTodoMutation.builder().input(createTodoInput).build()).enqueue(mutationCallback);
        }
    }

    private GraphQLCall.Callback<CreateTodoMutation.Data> mutationCallback = new GraphQLCall.Callback<CreateTodoMutation.Data>() {
        @Override
        public void onResponse(@Nonnull Response<CreateTodoMutation.Data> response) {
            try {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(MainActivity.this, "Added ToDo", Toast.LENGTH_SHORT).show();
                    }
                });
                Log.i(TAG, "Added Todo on AWS Cloud");
            } catch (Exception e) {
                Log.e(TAG, "mutationCallback onResponse: " + e.getMessage());
            }
        }

        @Override
        public void onFailure(@Nonnull ApolloException e) {
            Log.e(TAG, "mutationCallback onFailure: " + e.getMessage());
        }
    };

    public void runQuery() {
        mAWSAppSyncClient.query(ListTodosQuery.builder().build())
                .responseFetcher(AppSyncResponseFetchers.CACHE_AND_NETWORK)
                .enqueue(todosCallback);
    }

    private GraphQLCall.Callback<ListTodosQuery.Data> todosCallback = new GraphQLCall.Callback<ListTodosQuery.Data>() {
        @Override
        public void onResponse(@Nonnull Response<ListTodosQuery.Data> response) {
            try {
                if (null != response.data()) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(MainActivity.this, "Congratulations! Got ToDos.", Toast.LENGTH_SHORT).show();
                        }
                    });
                    Log.i(TAG, "Results: " + response.data().listTodos().items().toString());
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(MainActivity.this, "No Results!!!", Toast.LENGTH_SHORT).show();
                        }
                    });
                    Log.i(TAG, "No Results!!!");
                }
            } catch (Exception e) {
                Log.e(TAG, "onResponse: " + e.getMessage());
            }
        }

        @Override
        public void onFailure(@Nonnull ApolloException e) {
            try {
                Log.e(TAG, "ERROR: " + e.getMessage());
            } catch (Exception ex) {
                Log.e(TAG, "onFailure: " + e.getMessage());
            }
        }
    };
}
